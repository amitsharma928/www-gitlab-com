---
layout: handbook-page-toc
title: "GitLab.com SaaS Realm Labels and Tags"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Quick Links

* [Infrastructure Standards - Sandbox Realm Documentation](/handbook/infrastructure-standards/realms/sandbox/)
* [Infrastructure Standards - Global Labels and Tags Standards](/handbook/infrastructure-standards/labels-tags/)
* [Infrastructure Standards - Policies](/handbook/infrastructure-standards/policies/)
* [Infrastructure Standards - Tutorials](/handbook/infrastructure-standards/tutorials/)
* [Infrastructure Standards - Helpdesk](/handbook/infrastructure-standards/helpdesk/)

## Quick Reference

We use the `gl_` prefix for all labels and tags. All keys use underscores (`snake_case`). All values should use hyphens (`alpha-dash` for slug'd values), however underscores are allowed.

### Global Labels/Tags

See the [list of global labels and tags](/handbook/infrastructure-standards/labels-tags/) that should be applied to each resource.

### Realm Labels/Tags

There are no labels and tags specific to this realm yet.
