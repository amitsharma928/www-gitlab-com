---
layout: handbook-page-toc
title: "Processing SOC2 Requests"
description: "This Handbook page covers how to request a copy of the SOC2 report as well as the procedure for responding to SOC2 Requests"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
[Security Team](/handbook/engineering/security/) > [Field Security Team Page](/handbook/engineering/security/security-assurance/risk-field-security/index.html) > [Field Security Procedures](/handbook/engineering/security/security-assurance/risk-field-security/procedures/index.html)

# GitLab's SOC2 Request/Response Process
GitLab's [Risk and Field Security Team](/handbook/engineering/security/security-assurance/risk-field-security/index.html) is responsible for the Triage and response to [SOC2 Report Requests](/handbook/engineering/security/security-assurance/security-compliance/certifications.html#requesting-a-copy-of-the-gitlab-soc2-type-2-report)

This Handbook page covers how to [request a copy of the SOC2 report](#requesting-a-soc2-report) as well as the procedure for [responding to SOC2 Requests](#responding-to-a-soc2-request---risk-and-field-security)
- Requesting a SOC2 Report Instructions for:
    - [Customers and Prospects](#requesting-a-soc2-report---customers-and-prospects)
    - [GitLab Team Members](#requesting-a-soc2-report---gitlab-team-members)

## Requesting a SOC2 Report
### End to End Workflow

```mermaid
graph LR
    sub[[Submit Request for<br/>SOC2 report]]
    triage[[Triage the Request]]
    respond[[Respond to the<br/>Initial Request]]
    cansend{Can the report be sent?}
    sub-->|Requester<br/>--GL Sales<br/>OR<br/>Customer--|triage
    triage-->|Risk & FS<br/>OR<br/>GL Sales|respond
    respond-->cansend-->yes-->|Risk & FS|send[[Prepare and Send<br/>Report]]
```


### Requesting a SOC2 Report - Customers and Prospects
#### Workflow - Requesting a SOC2 Report (Customers and Prospects)

```mermaid
graph LR
    customer{Are You a Current Paying Customer?}
    customer-->|Requester|yescust(Yes)
    customer-->|Requester|nocust(No)
    yescust-->|Requester|sub[[Contact Security with<br/>SOC2 Request]]
    nocust-->|Requester|prospect{Are you evaluating GitLab<br/>and working with a<br/>GitLab Sales Team?}
    prospect-->|Requester|yesteam[Yes]
    prospect-->|Requester|noteam[No]
    yesteam-->|Requester|contact[[Contact your Representative<br/>with your request]]
    noteam-->|Requester|start[[Contact GitLab Sales<br/>https://about.gitlab.com/sales/ ]]
```

#### Procedure - Requesting a SOC2 Report (Customers and Prospects)
1. Determine the appropriate course of action from the above workflow, it will either be:
    - Contact your GitLab Representative team with your request
        - This would be your known point of contact at GitLab such as a **Technical Account Manger**, **Account Executive**, or **Solutions Architect**
    - [Contact Sales](https://about.gitlab.com/sales/) to become a valid entity in our Customer Relationship management System
    - Submit a SOC2 request to the Risk and Field Security Team
1. Contact the entity determined in the above step
- **Contacting the Security Team** - Use the below template for contacting the Field Security team with a request for a SOC2 Report:

```
To: security@gitlab.com
Subject: Requesting GitLab's SOC2 report - {Entity Name}
Body:
Hello GitLab Security I am requesting the SOC2 report for {Business/Entity Name}.

Details for my request are as follows:
Company/Account Name: {Company or Account Name}
GitLab Administrator/System Owner at Company: {System Owner/Administrator at your Business/Entity}
GitLab Administrator Email: {systemadministrator@company.com}
Individual To Receive Report: {First Name Last Name}
Individual's Job Title: {Information Security Analyst, CEO, etc...}
Individual's Email Address: {requesteremail@company.com}
```


### Requesting a SOC2 Report - GitLab Team Members
#### Workflow - Requesting a SOC2 Report - GitLab Team Members

```mermaid
graph LR
    confidentiality{Are Confidentiality Protections<br/>in place?<br/>--Current Terms or NDA--}
    confidentiality-->|Requester<br/>--in SFDC--|yesconf(Yes)
    confidentiality-->|Requester<br/>--in SFDC--|noconf(No)
    yesconf-->|Requester|sub[[Submit a SOC2<br/>Request]]
    noconf-->|Requester|nda[[Ensure an NDA is<br/>executed and on file]]
    nda-->|Requester|sub
    workflow[Slack Workflow Automation<br/>SOC2 Request<br/>#sec-fieldsecurity]
    gform[Google Form:<br/>SOC2 Request]
    informal[Informal Request:<br/>Slack #sec-fieldsecurity]
    sub-.-workflow
    sub-.-gform
    sub-.-informal
```

#### Procedure - Requesting a SOC2 Report (GitLab Team Members)
1. Verify that the Account is in SFDC
1. Verify that either:
    - The Account is an Active Customer, covered under Terms
    - The Account has an Active/Valid NDA on file
1. Submit a request for the SOC2 report to the Risk and Field Security Team
    - **PREFERRED** Slack Workflow Automation in [#sec-fieldsecurity](https://gitlab.slack.com/archives/CV5A53V70)
        - [ ] Open the [#sec-fieldsecurity](https://gitlab.slack.com/archives/CV5A53V70) slack channel
        - [ ] Expand the [Slack Workflows](https://slack.com/features/workflow-automation) Menu
        - [ ] Select the `Request a SOC2 Report` workflow
        - [ ] Follow the prompts
    - [Google Form: SOC2 Request](https://forms.gle/ZufNgcPtKUFc65326) - Fill out the fields
    - **BACKUP** Send a request in the [#sec-fieldsecurity](https://gitlab.slack.com/archives/CV5A53V70) Slack channel using the following text
    
    ```
    @field-security can I get a SOC2 Report for {customer name}
    Account Name: {Account}
    SFDC Link: {Account or opportunity link}
    Individual Name: {First Last}
    Individual Title: {Job Title}
    Individual Email: {person@account.com}
    Account Owner: {Account Owner}
    Account Owner Email: {accountowner@gitlab.com}
    CC Individuals: {…..@gitlab.com, ----@gitlab.com, *****@gitlab.com)
    ```

## Responding to a SOC2 Request - Risk and Field Security
### Field Security - Triaging The Request
To process a request we need to triage and verify that we can fulfill the request

```mermaid
graph LR
    rec[Recieve the Request]
    sf{Account in SFDC?}
    cur{Current Paying Customer<br/>Under Terms?}
    nda{Current NDA in place?}
    ex[[Execute and<br/>file NDA<br/>--in SFDC--]]
    ps[[Package and Send<br/>SOC2 Report]]
    redir[[Redirect to sales:<br/>https://about.gitlab.com/sales]]
    rec-->|Recipient|sf
    sf-->yes
    sf-->no-->|Recipient|redir
    yes-->cur-->curyes(Yes)-->|Risk & FS|ps
    cur-->curno(No)-->|In SFDC|nda
    nda-->nday(Yes)-->|Risk & FS|ps
    nda-->ndan(No)-->|Account Team|ex-->|Risk & FS|ps

```

#### Valid Request Sources
The following are valid sources for receiving a SOC2 request:
1. [Slack: #sec-fieldsecurity](https://gitlab.slack.com/archives/CV5A53V70)
1. Zendesk Queue: `security@gitlab.com`
1. Issue on the SA Triage Boards

#### Valid Requesters
1. Account Owner - GitLab Team Member
1. Solutions Architect - GitLab Team Member
1. Technical Account Manager - GitLab Team Member
1. Employee of Eligible Account
- **NOTE** - GitLab will not deliver SOC2 report to an auditor of an Eligible Account; in this case an information security consultant from the Eligible Account is responsible to request the report and appropriately present to their auditing firm

### Field Security - Responding to a Valid Request
1. Send Initial Response 
    - Request Receieved from Customer/Prospect **via Zendesk** - [Verified Entity under NDA](https://gitlab.com/gitlab-com/fs-snippets/-/snippets/2033339)
        - Input Request into Slack Workflow Automation in [#sec-fieldsecurity](https://gitlab.slack.com/archives/CV5A53V70) slack channel then follow the Slack SOC2 Request Procedure
    - Request Receieved from GitLab Team Member **via Slack** - Follow Slack SOC2 Request Procedure
    - Request Receieved from GitLab Team Member **via Google Form** - Input Request into Slack Workflow Automation in [#sec-fieldsecurity](https://gitlab.slack.com/archives/CV5A53V70) slack channel then follow the Slack SOC2 Request Procedure
1. Capture the Request in the Risk and Field Security Metrics Spreadsheet
1. Run the [**Internal Only** - SOC2 Watermarker](https://gitlab.com/-/ide/project/gitlab-private/sec-compliance/field_security/watermarker/tree/master/-/resources/request.txt/) following the [**Internal Only** - Instructions](https://gitlab.com/gitlab-private/sec-compliance/field_security/watermarker/-/blob/master/runbook-gitlabci.md)
1. Send the SOC2 Report per the [**Internal Only** - Instructions](https://gitlab.com/gitlab-private/sec-compliance/field_security/watermarker/-/blob/master/runbook-gitlabci.md) in the Watermarker runbook

### Field Security - Responding to an Invalid Request
#### Responding to an Invalid Request - SFDC Account but NO NDA
1. Send the Initial Response
    - Request Receieved from Customer/Prospect **via Zendesk** - [Verified Entity, NO NDA on file](https://gitlab.com/gitlab-com/fs-snippets/-/snippets/2033340)
    - Request Receieved from a GitLab Team Member - Inform them an NDA needs to be executed first

#### Responding to an Invalid Request - No SFDC Account
1. Send the Initial Response
    - Request Receieved from Customer/Prospect **via Zendesk** - [No Entity Found in SFDC](https://gitlab.com/gitlab-com/fs-snippets/-/snippets/2033341)
    - Request Receieved from GitLab Team member - Inform the Team Member that we need a valid account in SFDC before we can proceed
