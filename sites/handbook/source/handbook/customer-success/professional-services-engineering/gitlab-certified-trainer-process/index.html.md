---
layout: handbook-page-toc
title: "GitLab Certified Trainer - Candidate Process"
description: "Explore how GitLab Professional Services certifies trainers to validate their readiness to deliver Education Services offerings."

---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## GitLab Certified Trainer - Candidate Process

Professional Services now has a certification process for trainers to validate their readiness to deliver Education Services offerings. Here are the steps required to earn the certification for a given course. 


**Step 1**: [Create a new issue](https://gitlab.com/gitlab-com/customer-success/professional-services-group/education-services/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#) in the Education Services namespace using the `certified_trainer_candidate` issue template. If you are a partner or customer, contact your GitLab representative to gain access to the namespace or if you need assistance with creating the issue.

**Step 2**: Complete each item listed in the Candidate Tasks section of the issue description. As you work through the tasks, reach out to the training coordinator listed in the issue to schedule your shadowing and evaluation sessions. Here are the key tasks you will need to complete.
- Review the train-the-trainer (T3) resources: Attend a live session and/or review T3 materials asychronously
- Shadow a certified trainer delivering the course to customers
- Pass the end-user certification assessments for the course
- Submit a recording of you delivering a "dry-run" portion of the course 
- Deliver the entire course to a customer with a certified trainer observing or reviewing the recording, and receive an average rating of at least 4 out of 5 on the customer Training Survey.

**Step 3**: Once you are certified, add the certification to your LinkedIn profile!
